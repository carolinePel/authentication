const express = require('express')
const bodyParser = require('body-parser')
const users = require('../routes/users')

const app = express()


app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.use('/users',users)
// app.post('/send', (req, res) => {
//   console.log(req.body);
//   res.send(
//     `This is what you sent me: ${req.body.new}`,
//   )
// })

module.exports = app