

const app = require('./app/index')
var db = require('./db/index')
const dotenv = require('dotenv').config()


const port = process.env.PORT || '5000'

app.listen(port, () => console.log(`Listening on port ${port}`))

// db.connect(url,(err) => {
//     if(err) {
//         console.log('unable to connect to database')
//         process.exit(1)
//     }
//     else{
//         app.listen(port, () => console.log(`Listening on port ${port}`))
//     }
// })

