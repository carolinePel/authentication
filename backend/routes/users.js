var express = require('express')
var router = express.Router()
var users = require('../db/users')
var assert = require('assert')

router
  .get('/', (req, res) => {
    res.send(users.get());
  })
  .post('/', (req, res) => {
    const newUser = { "name": req.body.new }
    users
      .get()
      .insertOne(newUser,(err,result) => {
        assert.equal(null,err)
        res.send(
          `insert successful: ${req.body.new}`,
        )
      })
  })
  .post('/update', (req, res) => {
    console.log("update", req, res)
  })



module.exports = router;