import React from 'react'
import './customButton.scss'

const CustomButton = ({color, action, value, handleClick, children}) => (<button 
    className="customButton"
    style={{backgroundColor:color}}
    data-action={action}
    onClick={handleClick(value)}
  >
    {children}
  </button>)

export default CustomButton

