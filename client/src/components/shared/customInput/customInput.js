import React from 'react'
import "./customInput.scss"

const CustomInput = ({value,onChange}) => (
  <input
    className="customInput"
    type="text"
    value={value}
    onChange={onChange}
  />
  )
  export default CustomInput