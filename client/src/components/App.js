import React, { Component } from 'react';
import './App.css';
import Welcome from './Welcome'

class App extends Component {

  state = {
    responseToPost: '',
    tab:'welcome'
  }

  componentDidMount(){
    this.callApi()
    .then(res => this.setState({ users: res }))
    .then(() => console.log("state",this.state))
    .catch(err => console.log("error",err))
  }

  componentDidUpdate(state){
    console.log("update component",this.state)
  }

  callApi = async () =>{
    const response = await fetch('/users')
    const body = await response.json()

    if(response.status !== 200)throw Error(body.message)
    return body
  }
  
  current = tab => {
    if(tab === 'welcome'){
        return <Welcome/>
    }
    else if(tab === 'login'){
        return <p>login</p>
    }
    else if(tab === 'logout'){
        return <p>logout</p>
    }
    return null
  }

  render() {
    const { tab,response } =  this.state
    return this.current(tab)
  }
}

export default App;
